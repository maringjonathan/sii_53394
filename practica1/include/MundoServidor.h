// Mundo.h: interface for the CMundo class.
//
//////////////////////////////////////////////////////////////////////

#if !defined(AFX_MUNDO_H__9510340A_3D75_485F_93DC_302A43B8039A__INCLUDED_)
#define AFX_MUNDO_H__9510340A_3D75_485F_93DC_302A43B8039A__INCLUDED_

#include <vector>
#include "Plano.h"
#include <iostream>
#include <chrono>
typedef std::chrono::high_resolution_clock Clock;

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#include <pthread.h>
#include "DatosMemCompartida.h"
class CMundo  
{
        Clock::time_point t1, t2;
	DatosMemCompartida mem;
	DatosMemCompartida* pmem;
	int fdservidor, fdcs, fdsc;
	char* fifo = "/tmp/fifo_servidor";
	char* fifocs = "/tmp/fifo_cs";
	char* fifosc = "/tmp/fifo_sc";
	pthread_t thid1;
 
public:
	void RecibeComandosJugador();
	void Init();
	CMundo();
	virtual ~CMundo();	
	
	void InitGL();	
	void OnKeyboardDown(unsigned char key, int x, int y);
	void OnTimer(int value);
	void OnDraw();	

	Esfera esfera;
	std::vector<Plano> paredes;
	Plano fondo_izq;
	Plano fondo_dcho;
	Raqueta jugador1;
	Raqueta jugador2;

	int puntos1;
	int puntos2;
};

#endif // !defined(AFX_MUNDO_H__9510340A_3D75_485F_93DC_302A43B8039A__INCLUDED_)
