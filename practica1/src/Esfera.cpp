// Esfera.cpp: implementation of the Esfera class.
//
//////////////////////////////////////////////////////////////////////

#include "Esfera.h"
#include "glut.h"
//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////

Esfera::Esfera()
{
	r = g = 150;

	b = 0;

	radio=0.5f;

	velocidad.x=3;

	velocidad.y=3;
}

Esfera::~Esfera()
{

}



void Esfera::Dibuja()
{

	glColor3ub(255,255,0);
	glEnable(GL_LIGHTING);
	glPushMatrix();
	glTranslatef(centro.x,centro.y,0);
	glutSolidSphere(radio,15,15);
	glPopMatrix();
}

void Esfera::Mueve(float t)
{
	if(r < 255) r++;

		else r = 0;

	if(radio >= 0.2f)radio = radio * 0.99;

		else radio = 0.8f;	

	centro = centro + velocidad*t;
}
