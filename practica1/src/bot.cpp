#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <sys/mman.h>
#include "DatosMemCompartida.h"
#include <unistd.h>


int main(){
	
	DatosMemCompartida* pmem;
	int fd;	

	fd = open("/tmp/mmap_juego", O_RDWR);
	pmem = (DatosMemCompartida *) mmap(NULL, sizeof(&pmem), PROT_READ|PROT_WRITE, MAP_SHARED,fd,0);
	close(fd);

	while(1){
		usleep(25000);
		if(pmem->esfera.centro.y > (pmem->raqueta1.y1 + pmem->raqueta1.y2)/2 )
			pmem->accion = 1;
		else if(pmem->esfera.centro.y < (pmem->raqueta1.y1 + pmem->raqueta1.y2)/2 )
			pmem->accion = -1;
		else
			pmem->accion = 0;
		printf("%f\n",pmem->esfera.centro.y);
	};
	return 0;
}
